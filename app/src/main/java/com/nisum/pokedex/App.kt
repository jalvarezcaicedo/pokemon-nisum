package com.nisum.pokedex

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

@HiltAndroidApp
class App : Application() {

    val applicationScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    companion object {
        private var instance: App? = null

        @JvmStatic
        @Synchronized
        fun getInstance(): App = instance!!
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}