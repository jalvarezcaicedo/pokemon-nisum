package com.nisum.pokedex.di

import android.app.Application
import com.nisum.pokedex.data.repository.PokedexRepository
import com.nisum.pokedex.data.repository.PokemonRepository
import com.nisum.pokedex.data.source.LocalPokedexDataSource
import com.nisum.pokedex.data.source.LocalPokemonDataSource
import com.nisum.pokedex.data.source.RemotePokedexDataSource
import com.nisum.pokedex.data.source.RemotePokemonDataSource
import com.nisum.pokedex.framework.database.PokedexDB
import com.nisum.pokedex.framework.database.source.LocalPokedexDataSourceImpl
import com.nisum.pokedex.framework.database.source.LocalPokemonDataSourceImpl
import com.nisum.pokedex.framework.server.service.PokeApiService
import com.nisum.pokedex.framework.server.source.RemotePokedexDataSourceImpl
import com.nisum.pokedex.framework.server.source.RemotePokemonDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): PokedexDB = PokedexDB.getDatabase(app)

    @Provides
    fun pokedexRepository(
        localPokedexDataSource: LocalPokedexDataSource,
        remotePokedexDataSource: RemotePokedexDataSource
    ) = PokedexRepository(localPokedexDataSource, remotePokedexDataSource)

    @Provides
    fun pokemonRepository(
        localPokemonDataSource: LocalPokemonDataSource,
        remotePokemonDataSource: RemotePokemonDataSource
    ) = PokemonRepository(localPokemonDataSource, remotePokemonDataSource)

    @Provides
    fun localPokedexDataSourceProvider(
        db: PokedexDB
    ): LocalPokedexDataSource = LocalPokedexDataSourceImpl(db)

    @Provides
    fun localPokemonDataSourceProvider(
        db: PokedexDB
    ): LocalPokemonDataSource = LocalPokemonDataSourceImpl(db)

    @Provides
    fun remotePokedexDataSourceProvider(
        pokeApiService: PokeApiService
    ): RemotePokedexDataSource = RemotePokedexDataSourceImpl(pokeApiService)

    @Provides
    fun remotePokemonDataSourceProvider(
        pokeApiService: PokeApiService
    ): RemotePokemonDataSource = RemotePokemonDataSourceImpl(pokeApiService)

}
