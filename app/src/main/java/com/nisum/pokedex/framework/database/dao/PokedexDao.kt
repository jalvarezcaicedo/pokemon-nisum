package com.nisum.pokedex.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nisum.pokedex.framework.database.entity.PokedexEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PokedexDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pokedexData: List<PokedexEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemon(pokedex: PokedexEntity)

    @Query("SELECT * FROM Pokedex ORDER BY id ASC")
    fun findPokemonOrderById(): Flow<List<PokedexEntity>>

    @Query("SELECT * FROM Pokedex ORDER BY name ASC")
    fun findPokemonOrderByName(): Flow<List<PokedexEntity>>

    @Query("SELECT * FROM Pokedex WHERE name LIKE '%'||:query||'%'")
    fun findPokemonFiltered(query: String): Flow<List<PokedexEntity>>

}