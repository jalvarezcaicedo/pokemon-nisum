package com.nisum.pokedex.framework.server.response

import com.google.gson.annotations.SerializedName

data class EvolutionChainServer(
    @SerializedName("evolution_chain")
    val evolutionChain: EvolutionChain
)

data class EvolutionChain(
    val url: String
)
