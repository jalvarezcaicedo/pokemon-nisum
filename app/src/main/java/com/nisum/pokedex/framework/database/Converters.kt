package com.nisum.pokedex.framework.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nisum.domain.common.AbilityData
import com.nisum.domain.common.Location
import com.nisum.domain.common.Moves
import com.nisum.domain.common.TypeData

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun List<TypeData>.typeDataToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toTypeData(): List<TypeData> =
        gson.fromJson(this, object : TypeToken<List<TypeData>?>() {}.type)

    @TypeConverter
    fun List<Moves>.moveListToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toMoveList(): List<Moves> =
        gson.fromJson(this, object : TypeToken<List<Moves>?>() {}.type)

    @TypeConverter
    fun List<Location>.locationListToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toLocationList(): List<Location> =
        gson.fromJson(this, object : TypeToken<List<Location>?>() {}.type)

    @TypeConverter
    fun List<AbilityData>.abilityDataToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toAbilityDataList(): List<AbilityData> =
        gson.fromJson(this, object : TypeToken<List<AbilityData>?>() {}.type)

}