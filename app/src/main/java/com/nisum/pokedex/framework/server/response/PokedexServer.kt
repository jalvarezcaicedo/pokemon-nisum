package com.nisum.pokedex.framework.server.response

import com.google.gson.annotations.SerializedName
import com.nisum.domain.common.Pokedex

data class PokedexServer(
    @SerializedName("results")
    var results: List<Pokedex>
)


