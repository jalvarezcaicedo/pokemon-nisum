package com.nisum.pokedex.framework.server

import com.nisum.domain.PokedexDomain
import com.nisum.domain.PokemonDomain
import com.nisum.domain.common.Location
import com.nisum.domain.common.Pokedex
import com.nisum.pokedex.framework.server.response.EncountersServer
import com.nisum.pokedex.framework.server.response.EvolutionChainServer
import com.nisum.pokedex.framework.server.response.EvolutionServer
import com.nisum.pokedex.framework.server.response.PokemonServer

fun Pokedex.toPokedexDomain(id: Long) = PokedexDomain(
    id = id,
    name = name
)

fun PokemonServer.toPokemonDomain() = PokemonDomain(
    id,
    name,
    abilityData,
    evolve,
    locationArea,
    typeData,
    moves
)

fun List<EncountersServer>.toPokemonDomain(pokemonDomain: PokemonDomain): PokemonDomain {
    val locations: MutableList<Location> = mutableListOf()
    this.forEach { location ->
        locations.add(Location(location.locationArea.name))
    }
    pokemonDomain.locationArea = locations
    return pokemonDomain
}

fun EvolutionServer.toPokemonDomain(pokemonDomain: PokemonDomain): PokemonDomain {
    var pokemonEvolves = chain.species.name
    if (chain.evolvesTo.isNotEmpty()) {
        pokemonEvolves = pokemonEvolves.plus(" > ").plus(chain.evolvesTo[0].species.name)
        if (chain.evolvesTo[0].evolvesTo.isNotEmpty()) pokemonEvolves =
            pokemonEvolves.plus(" > ").plus(chain.evolvesTo[0].evolvesTo[0].species.name)
    }
    pokemonDomain.evolve = pokemonEvolves
    return pokemonDomain
}

fun EvolutionChainServer.getEvolutionChainId(): Int {
    var evolutionChainId =
        evolutionChain.url.replace("https://pokeapi.co/api/v2/evolution-chain/", "", true)
    evolutionChainId = evolutionChainId.replace("/", "")
    return evolutionChainId.toInt()
}