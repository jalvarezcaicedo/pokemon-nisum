package com.nisum.pokedex.framework.database

import com.nisum.domain.PokedexDomain
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.framework.database.entity.PokedexEntity
import com.nisum.pokedex.framework.database.entity.PokemonEntity

fun PokedexDomain.toPokedexEntity() = PokedexEntity(
    id,
    name
)

fun List<PokedexEntity>.toPokedexDomainList(): List<PokedexDomain> = map {
    it.toPokedexDomain()
}

fun PokedexEntity.toPokedexDomain() = PokedexDomain(
    id,
    name
)

fun PokemonDomain.toPokemonEntity() = PokemonEntity(
    id = id,
    name = name,
    abilities,
    evolve = evolve,
    locationArea = locationArea,
    types = types,
    moves = moves
)

fun PokemonEntity.toPokemonDomain() = PokemonDomain(
    id,
    name,
    abilities,
    evolve,
    locationArea,
    types,
    moves
)