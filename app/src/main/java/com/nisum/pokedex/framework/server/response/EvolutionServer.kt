package com.nisum.pokedex.framework.server.response

import com.google.gson.annotations.SerializedName

data class EvolutionServer(
    val chain: Chain
)

data class Chain(
    @SerializedName("evolves_to")
    val evolvesTo: List<EvolvesTo>,
    val species: Species
)

data class EvolvesTo(
    @SerializedName("evolves_to")
    val evolvesTo: List<EvolvesTo>,
    val species: Species
)

data class Species(
    val name: String
)
