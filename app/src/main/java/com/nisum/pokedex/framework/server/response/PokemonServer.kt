package com.nisum.pokedex.framework.server.response

import com.google.gson.annotations.SerializedName
import com.nisum.domain.common.AbilityData
import com.nisum.domain.common.Location
import com.nisum.domain.common.Moves
import com.nisum.domain.common.TypeData

data class PokemonServer(
    val id: Int,
    val name: String,
    @SerializedName("abilities")
    val abilityData: List<AbilityData>,
    var evolve: String?,
    var locationArea: List<Location>?,
    @SerializedName("types")
    val typeData: List<TypeData>,
    val moves: List<Moves>
)






