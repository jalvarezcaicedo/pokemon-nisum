package com.nisum.pokedex.framework.database.source

import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.source.LocalPokedexDataSource
import com.nisum.pokedex.framework.database.PokedexDB
import com.nisum.pokedex.framework.database.toPokedexDomainList
import com.nisum.pokedex.framework.database.toPokedexEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalPokedexDataSourceImpl(
    private val db: PokedexDB
) : LocalPokedexDataSource {

    private val pokedexDao by lazy { db.pokedexDao() }

    override fun getLocalPokedexDataById(): Flow<List<PokedexDomain>> =
        pokedexDao.findPokemonOrderById().map {
            it.toPokedexDomainList()
        }

    override fun getLocalPokedexDataByName(): Flow<List<PokedexDomain>> =
        pokedexDao.findPokemonOrderByName().map {
            it.toPokedexDomainList()
        }

    override fun getLocalPokedexDataFiltered(query: String): Flow<List<PokedexDomain>> =
        pokedexDao.findPokemonFiltered(query).map {
            it.toPokedexDomainList()
        }

    override suspend fun savePokedex(pokedex: List<PokedexDomain>) {
        pokedexDao.insertAll(pokedex.map { it.toPokedexEntity() })
    }

}