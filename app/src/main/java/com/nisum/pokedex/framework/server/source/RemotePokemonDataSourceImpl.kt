package com.nisum.pokedex.framework.server.source

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.source.RemotePokemonDataSource
import com.nisum.pokedex.framework.server.getEvolutionChainId
import com.nisum.pokedex.framework.server.service.PokeApiService
import com.nisum.pokedex.framework.server.toPokemonDomain
import com.nisum.pokedex.framework.util.tryCall

class RemotePokemonDataSourceImpl(
    private val pokeApiService: PokeApiService
) : RemotePokemonDataSource {

    override suspend fun getPokemonData(id: Int): Either<Error, PokemonDomain> = tryCall {
        pokeApiService.getPokemonData(id).toPokemonDomain()
    }

    override suspend fun getPokemonLocationData(pokemonDomain: PokemonDomain): Either<Error, PokemonDomain> =
        tryCall {
            pokeApiService.getPokemonEncountersData(pokemonDomain.id).toPokemonDomain(pokemonDomain)
        }

    override suspend fun getPokemonEvolveData(
        pokemonDomain: PokemonDomain,
        chainEvolveId: Int
    ): Either<Error, PokemonDomain> =
        tryCall {
            pokeApiService.getPokemonEvolutionData(chainEvolveId).toPokemonDomain(pokemonDomain)
        }

    override suspend fun getPokemonEvolveChainData(pokemonId: Int): Either<Error, Int> = tryCall {
        pokeApiService.getEvolutionChainData(pokemonId).getEvolutionChainId()
    }

}