package com.nisum.pokedex.framework.server.response

import com.google.gson.annotations.SerializedName

data class EncountersServer(
    @SerializedName("location_area")
    val locationArea: LocationArea
)

data class LocationArea(
    val name: String
)
