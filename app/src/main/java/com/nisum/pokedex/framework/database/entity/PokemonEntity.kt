package com.nisum.pokedex.framework.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nisum.domain.common.AbilityData
import com.nisum.domain.common.Location
import com.nisum.domain.common.Moves
import com.nisum.domain.common.TypeData

@Entity(tableName = "Pokemon")
data class PokemonEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val abilities: List<AbilityData>,
    var evolve: String?,
    var locationArea: List<Location>?,
    val types: List<TypeData>,
    val moves: List<Moves>
)
