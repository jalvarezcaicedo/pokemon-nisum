package com.nisum.pokedex.framework.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.nisum.pokedex.framework.database.dao.PokedexDao
import com.nisum.pokedex.framework.database.dao.PokemonDao
import com.nisum.pokedex.framework.database.entity.PokedexEntity
import com.nisum.pokedex.framework.database.entity.PokemonEntity

@Database(
    entities = [
        PokedexEntity::class,
        PokemonEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class PokedexDB : RoomDatabase() {

    abstract fun pokedexDao(): PokedexDao
    abstract fun pokemonDao(): PokemonDao

    companion object {
        @Synchronized
        fun getDatabase(context: Context): PokedexDB = Room.databaseBuilder(
            context.applicationContext,
            PokedexDB::class.java,
            "pokedex_db"
        ).build()
    }
}