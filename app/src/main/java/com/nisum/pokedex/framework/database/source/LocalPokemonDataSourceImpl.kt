package com.nisum.pokedex.framework.database.source

import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.source.LocalPokemonDataSource
import com.nisum.pokedex.framework.database.PokedexDB
import com.nisum.pokedex.framework.database.toPokemonDomain
import com.nisum.pokedex.framework.database.toPokemonEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalPokemonDataSourceImpl(
    private val db: PokedexDB
) : LocalPokemonDataSource {

    private val pokemonDao by lazy { db.pokemonDao() }

    override fun getLocalPokemonData(id: Int): Flow<PokemonDomain> =
        pokemonDao.getPokemonDetail(id).map { it.toPokemonDomain() }

    override suspend fun savePokemon(pokemon: PokemonDomain) {
        pokemonDao.insertPokemon(pokemon.toPokemonEntity())
    }
}