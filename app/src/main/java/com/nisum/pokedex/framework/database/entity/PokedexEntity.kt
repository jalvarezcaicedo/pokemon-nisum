package com.nisum.pokedex.framework.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Pokedex")
data class PokedexEntity(
    @PrimaryKey
    val id: Long,
    val name: String
)
