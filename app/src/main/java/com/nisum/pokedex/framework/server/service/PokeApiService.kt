package com.nisum.pokedex.framework.server.service

import com.nisum.pokedex.framework.server.response.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokeApiService {

    @GET("api/v2/pokemon")
    suspend fun getPokedexData(
        @Query("offset") offset: Long,
        @Query("limit") limit: Long
    ): PokedexServer

    @GET("api/v2/pokemon/{pokemonId}")
    suspend fun getPokemonData(
        @Path("pokemonId") pokemonId: Int
    ): PokemonServer

    @GET("api/v2/pokemon-species/{pokemonId}")
    suspend fun getEvolutionChainData(
        @Path("pokemonId") pokemonId: Int
    ): EvolutionChainServer

    @GET("api/v2/evolution-chain/{chainEvolutionId}")
    suspend fun getPokemonEvolutionData(
        @Path("chainEvolutionId") pokemonId: Int
    ): EvolutionServer

    @GET("api/v2/pokemon/{pokemonId}/encounters")
    suspend fun getPokemonEncountersData(
        @Path("pokemonId") pokemonId: Int
    ): List<EncountersServer>

}