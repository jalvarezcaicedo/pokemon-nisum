package com.nisum.pokedex.framework.server.source

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.source.RemotePokedexDataSource
import com.nisum.pokedex.framework.server.service.PokeApiService
import com.nisum.pokedex.framework.server.toPokedexDomain
import com.nisum.pokedex.framework.util.tryCall

class RemotePokedexDataSourceImpl(
    private val pokeApiService: PokeApiService
) : RemotePokedexDataSource {

    override suspend fun getPokedex(limit: Long): Either<Error, List<PokedexDomain>> = tryCall {
        var idGenerated: Long = 0
        pokeApiService.getPokedexData(0, limit).results.map {
            idGenerated += 1
            it.toPokedexDomain(idGenerated)
        }
    }

}