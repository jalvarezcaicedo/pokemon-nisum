package com.nisum.pokedex.feature.pokedex

import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.FragmentPokedexBinding
import com.nisum.pokedex.feature.MainViewModel
import com.nisum.pokedex.feature.common.BaseFragment
import com.nisum.pokedex.ktx.hideKeyboard
import com.nisum.pokedex.ktx.launchAndCollect
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber

@AndroidEntryPoint
class PokedexFragment : BaseFragment<FragmentPokedexBinding>(), PokedexItemListener,
    View.OnClickListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {

    private val pokedexAdapter: PokedexAdapter = PokedexAdapter(this)
    private val mainViewModel: MainViewModel by activityViewModels()
    private val viewModel: PokedexViewModel by viewModels()

    private val pokedexData = mutableListOf<PokedexDomain>()
    private var orderById = true

    override fun initView() {
        binding.rvPokedex.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@PokedexFragment.requireContext(), 3)
            adapter = pokedexAdapter
        }
    }

    override fun initListeners() {
        binding.ivLogoMenu.setOnClickListener(this)
        binding.ivOrderMenu.setOnClickListener(this)
        binding.svPokemonFinder.setOnQueryTextListener(this)
    }

    override fun initObservables() {
        with(viewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.remotePokedex }) {
                it?.let {
                    viewModel.savePokedexData(it)
                }
            }
            diff({ it.pokedexById }) {
                it?.let {
                    pokedexData.clear()
                    pokedexData.addAll(it)
                    binding.svPokemonFinder.setQuery("", false)
                    binding.svPokemonFinder.clearFocus()
                    handleUpdatePokedexData(it)
                }
            }
            diff({ it.pokedexByName }) {
                it?.let {
                    pokedexData.clear()
                    pokedexData.addAll(it)
                    binding.svPokemonFinder.setQuery("", false)
                    binding.svPokemonFinder.clearFocus()
                    handleUpdatePokedexData(it)
                }
            }
            diff({ it.pokedexFiltered }) {
                it?.let {
                    pokedexData.clear()
                    pokedexData.addAll(it)
                    handleUpdatePokedexData(it)
                }
            }
        }

        viewModel.getRemotePokedexData()
        viewModel.getPokedexById()
    }


    override fun getParametersFragment() {}

    override fun getViewBinding(): FragmentPokedexBinding =
        FragmentPokedexBinding.inflate(layoutInflater)

    override fun onSelectProductListener(pokemon: PokedexDomain) {
        val navAction = PokedexFragmentDirections.actionMenuPokedexToPokemonDetail(pokemon.id)
        findNavController().navigate(navAction)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivLogoMenu -> {
                mainViewModel.openMenuDrawer()
            }
            R.id.ivOrderMenu -> {
                val drawable = if (orderById) {
                    viewModel.getPokedexByName()
                    R.drawable.ic_order_name
                } else {
                    viewModel.getPokedexById()
                    R.drawable.ic_order_id
                }

                binding.ivOrderMenu.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        drawable
                    )
                )
                orderById = !orderById
            }

        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        hideKeyboard()
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText.isNullOrBlank()) if (orderById) viewModel.getPokedexById() else viewModel.getPokedexByName()
        else viewModel.getPokedexFiltered(newText)

        return true
    }

    private fun handleVisibility(loading: Boolean, data: List<PokedexDomain>) {
        Timber.d(loading.toString().plus(" $data"))
    }

    private fun handleUpdatePokedexData(pokedexData: List<PokedexDomain>) {
        handleVisibility(false, pokedexData)
        pokedexAdapter.submitPokedexData(pokedexData)
        binding.rvPokedex.smoothScrollToPosition(0)
    }

    private fun <T, U> Flow<T>.diff(mapf: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapf).distinctUntilChanged(),
            body = body
        )
    }

}