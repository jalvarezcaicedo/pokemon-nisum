package com.nisum.pokedex.feature.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.viewbinding.ViewBinding
import com.nisum.pokedex.R

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _binding: VB? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = getViewBinding()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getParametersFragment()
        initView()
        initListeners()
        initObservables()
    }

    abstract fun initView()

    abstract fun initListeners()

    abstract fun initObservables()

    abstract fun getParametersFragment()

    abstract fun getViewBinding(): VB

    fun addFragmentWithAnim(
        fragment: Fragment,
        @IdRes containerFragment: Int,
        nameFragment: String
    ) {
        val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            R.anim.enter_from_right,
            R.anim.exit_to_right,
            R.anim.enter_from_left,
            R.anim.exit_to_left
        )
        transaction.replace(containerFragment, fragment)
        transaction.addToBackStack(nameFragment)
        transaction.commit()
    }

    fun attachFragment(fragment: Fragment, container: Int, tag: String) {
        childFragmentManager
            .beginTransaction()
            .add(container, fragment, tag)
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .addToBackStack(null)
            .commit()
    }
}
