package com.nisum.pokedex.feature.pokemon

import android.graphics.drawable.GradientDrawable
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.BuildConfig
import com.nisum.pokedex.databinding.FragmentPokemonBinding
import com.nisum.pokedex.feature.common.BaseFragment
import com.nisum.pokedex.ktx.launchAndCollect
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber

@AndroidEntryPoint
class PokemonFragment : BaseFragment<FragmentPokemonBinding>() {

    private val typeAdapter: TypeAdapter = TypeAdapter()
    private val moveAdapter: MoveAdapter = MoveAdapter()
    private val viewModel: PokemonViewModel by viewModels()
    private val safeArgs: PokemonFragmentArgs by navArgs()
    private var pokemonId: Int = 1

    override fun initView() {
        Glide.with(requireContext()).load(
            BuildConfig.POKEMON_IMAGE_URL_BASE.plus(pokemonId).plus(".png")
        ).override(200, 200).into(binding.ivPokemonImage)

        binding.rvPokemonTypes.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                this@PokemonFragment.requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = typeAdapter
        }

        binding.rvPokemonMoves.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                this@PokemonFragment.requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = moveAdapter
        }
    }

    override fun initListeners() {
        binding.ivBackArrow.setOnClickListener { findNavController().popBackStack() }
    }

    override fun initObservables() {
        with(viewModel.state) {
            diff({ it.loading }) { handleVisibility(it, null) }
            diff({ it.remotePokemonData }) {
                it?.let {
                    viewModel.getLocalPokemonData(pokemonId)
                }
            }
            diff({ it.localPokemonData }) {
                it?.let {
                    updateUI(it)
                }
            }
        }

        viewModel.getRemotePokemonData(pokemonId)
        viewModel.getLocalPokemonData(pokemonId)
    }

    private fun updateUI(pokemonDomain: PokemonDomain) {
        binding.tvPokemonName.text = pokemonDomain.name
        binding.llHeader.setBackgroundColor(
            getTypeColor(
                requireContext(),
                pokemonDomain.types[0].type.name
            )
        )

        var drawable = view!!.background as GradientDrawable
        drawable.mutate()
        drawable.setStroke(10, getTypeColor(requireContext(), pokemonDomain.types[0].type.name))

        typeAdapter.submitPokemonTypeData(pokemonDomain)
        moveAdapter.submitPokemonMoveData(pokemonDomain)
        binding.tvEvolutionaryLine.text = pokemonDomain.evolve

        var abilities = ""
        var locations = ""

        pokemonDomain.abilities.forEach { ability ->
            abilities += "* ${ability.ability.name}\n"
        }

        pokemonDomain.locationArea?.forEach { location ->
            locations += "+ ${location.name}\n"
        }

        binding.tvPokemonAbilities.text = abilities
        binding.tvPokemonLocations.text = locations
    }

    override fun getParametersFragment() {
        pokemonId = safeArgs.pokemonId.toInt()
    }

    override fun getViewBinding(): FragmentPokemonBinding =
        FragmentPokemonBinding.inflate(layoutInflater)

    private fun handleVisibility(loading: Boolean, pokemonData: PokemonDomain?) {
        Timber.d(loading.toString().plus(" $pokemonData"))
    }

    private fun <T, U> Flow<T>.diff(mapf: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapf).distinctUntilChanged(),
            body = body
        )
    }

}