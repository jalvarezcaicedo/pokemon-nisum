package com.nisum.pokedex.feature.pokedex

import com.nisum.pokedex.data.repository.PokedexRepository
import com.nisum.usecase.pokedex.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class PokedexModule {

    @Provides
    @ViewModelScoped
    fun getRemotePokedexUseCase(
        pokedexRepository: PokedexRepository
    ) = GetRemotePokedexUseCase(pokedexRepository)

    @Provides
    @ViewModelScoped
    fun savePokedexDataUseCase(
        pokedexRepository: PokedexRepository
    ) = SavePokedexDataUseCase(pokedexRepository)

    @Provides
    @ViewModelScoped
    fun getLocalPokedexByIdUseCase(
        pokedexRepository: PokedexRepository
    ) = GetLocalPokedexByIdUseCase(pokedexRepository)

    @Provides
    @ViewModelScoped
    fun getLocalPokedexByNameUseCase(
        pokedexRepository: PokedexRepository
    ) = GetLocalPokedexByNameUseCase(pokedexRepository)

    @Provides
    @ViewModelScoped
    fun getLocalPokedexFiltered(
        pokedexRepository: PokedexRepository
    ) = GetLocalPokedexFilteredUseCase(pokedexRepository)

}