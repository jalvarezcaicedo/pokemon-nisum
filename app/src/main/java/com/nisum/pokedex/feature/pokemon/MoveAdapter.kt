package com.nisum.pokedex.feature.pokemon

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.nisum.domain.PokemonDomain
import com.nisum.domain.common.Move
import com.nisum.domain.common.Moves
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.ItemMoveBinding

class MoveAdapter : RecyclerView.Adapter<MoveAdapter.PokemonMoveViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<Moves>() {
        override fun areItemsTheSame(oldItem: Moves, newItem: Moves): Boolean {
            return oldItem.move.url == newItem.move.url
        }

        override fun areContentsTheSame(oldItem: Moves, newItem: Moves): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonMoveViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_move, parent, false)
        return PokemonMoveViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonMoveViewHolder, position: Int) {
        holder.bind(differ.currentList[position].move)
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submitPokemonMoveData(pokemonDomain: PokemonDomain) {
        differ.submitList(pokemonDomain.moves)
        notifyDataSetChanged()
    }

    inner class PokemonMoveViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemMoveBinding.bind(view)

        fun bind(item: Move) {
            binding.tvMoveName.text = "* ${item.name}"
        }
    }
}