package com.nisum.pokedex.feature

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> get() = _state.asStateFlow()

    fun resetState() {
        _state.update { UIState() }
    }

    fun openMenuDrawer() {
        _state.update { it.copy(openDrawer = true) }
    }

    fun resetOpenMenuDrawer() {
        _state.update { it.copy(openDrawer = null) }
    }

    data class UIState(
        val openDrawer: Boolean? = null
    )
}