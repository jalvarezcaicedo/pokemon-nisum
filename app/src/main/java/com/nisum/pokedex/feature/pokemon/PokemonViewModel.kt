package com.nisum.pokedex.feature.pokemon

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.usecase.pokemon.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonViewModel @Inject constructor(
    private val getRemotePokemonUseCase: GetRemotePokemonUseCase,
    private val getRemotePokemonLocationsUseCase: GetRemotePokemonLocationsUseCase,
    private val getRemotePokemonEvolveChainUseCase: GetRemotePokemonEvolveChainUseCase,
    private val getRemotePokemonEvolveUseCase: GetRemotePokemonEvolveUseCase,
    private val getLocalPokemonUseCase: GetLocalPokemonUseCase,
    private val savePokemonDataUseCase: SavePokemonDataUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getRemotePokemonData(id: Int) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemotePokemonUseCase(id).fold(
            ifLeft = { error ->
                _state.update { it.copy(error = error, loading = false) }

            },
            ifRight = { response ->
                _state.update { it.copy(loading = false) }
                getRemotePokemonLocation(response)
            }
        )
    }

    private fun getRemotePokemonLocation(pokemonDomain: PokemonDomain) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemotePokemonLocationsUseCase(pokemonDomain).fold(
            ifLeft = { error ->
                _state.update { it.copy(error = error, loading = false) }
            },
            ifRight = { response ->
                _state.update { it.copy(loading = false) }
                getRemotePokemonChainEvolveId(response)
            }
        )
    }


    private fun getRemotePokemonChainEvolveId(pokemonDomain: PokemonDomain) =
        viewModelScope.launch {
            _state.update { UIState(loading = true) }
            getRemotePokemonEvolveChainUseCase(pokemonDomain.id).fold(
                ifLeft = { error ->
                    _state.update { it.copy(error = error, loading = false) }
                },
                ifRight = { response ->
                    _state.update { it.copy(loading = false) }
                    getRemotePokemonEvolve(pokemonDomain, response)
                }
            )
        }

    private fun getRemotePokemonEvolve(pokemonDomain: PokemonDomain, chainEvolveId: Int) =
        viewModelScope.launch {
            _state.update { UIState(loading = true) }
            getRemotePokemonEvolveUseCase(pokemonDomain, chainEvolveId).fold(
                ifLeft = { error ->
                    _state.update { it.copy(error = error, loading = false) }
                },
                ifRight = { response ->
                    savePokemonData(response)
                    _state.update { it.copy(remotePokemonData = response, loading = false) }
                }
            )
        }

    fun getLocalPokemonData(id: Int) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalPokemonUseCase(id).catch {}.collect { pokemon ->
            _state.update { UIState(localPokemonData = pokemon, loading = false) }
        }
    }

    private fun savePokemonData(pokemonDomain: PokemonDomain) = viewModelScope.launch {
        savePokemonDataUseCase(pokemonDomain)
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val evolutionChainId: Int = 0,
        val localPokemonData: PokemonDomain? = null,
        val remotePokemonData: PokemonDomain? = null
    )

}