package com.nisum.pokedex.feature

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.ActivityMainBinding
import com.nisum.pokedex.ktx.launchAndCollect
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpDrawerLayout()
        initObservables()
    }

    override fun onBackPressed() {
        if (binding.drawerMenu.isDrawerOpen(GravityCompat.START)) {
            binding.drawerMenu.closeDrawer(GravityCompat.START)
        } else super.onBackPressed()
    }

    private fun setUpDrawerLayout() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.pokedexNavHostFragment) as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        navHostFragment.navController.graph = inflater.inflate(R.navigation.pokedex_graph)

        binding.navigationView.menu.clear()
        binding.navigationView.inflateMenu(R.menu.drawer_menu)

        val navController = navHostFragment.navController
        binding.navigationView.setupWithNavController(navController)
    }


    private fun initObservables() {
        with(mainViewModel.state) {
            diff({ it.openDrawer }) {
                it?.let {
                    binding.drawerMenu.openDrawer(GravityCompat.START)
                    mainViewModel.resetOpenMenuDrawer()
                }
            }
        }
    }

    private fun <T, U> Flow<T>.diff(mapf: (T) -> U, body: (U) -> Unit) {
        this@MainActivity.launchAndCollect(
            flow = map(mapf).distinctUntilChanged(),
            body = body
        )
    }

}