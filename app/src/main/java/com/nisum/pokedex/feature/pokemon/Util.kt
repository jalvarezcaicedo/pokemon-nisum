package com.nisum.pokedex.feature.pokemon

import android.content.Context
import androidx.core.content.ContextCompat
import com.nisum.pokedex.R

fun getTypeColor(context: Context, type: String): Int {
    when (type) {
        "rock" -> return ContextCompat.getColor(context, R.color.rockType)
        "ghost" -> return ContextCompat.getColor(context, R.color.ghostType)
        "steel" -> return ContextCompat.getColor(context, R.color.steelType)
        "water" -> return ContextCompat.getColor(context, R.color.waterType)
        "grass" -> return ContextCompat.getColor(context, R.color.grassType)
        "psychic" -> return ContextCompat.getColor(context, R.color.psychicType)
        "ice" -> return ContextCompat.getColor(context, R.color.iceType)
        "dark" -> return ContextCompat.getColor(context, R.color.darkType)
        "fairy" -> return ContextCompat.getColor(context, R.color.fairyType)
        "normal" -> return ContextCompat.getColor(context, R.color.normalType)
        "fight" -> return ContextCompat.getColor(context, R.color.fightType)
        "flying" -> return ContextCompat.getColor(context, R.color.flyingType)
        "poison" -> return ContextCompat.getColor(context, R.color.poisonType)
        "ground" -> return ContextCompat.getColor(context, R.color.groundType)
        "bug" -> return ContextCompat.getColor(context, R.color.bugType)
        "fire" -> return ContextCompat.getColor(context, R.color.fireType)
        "electric" -> return ContextCompat.getColor(context, R.color.electricType)
        "dragon" -> return ContextCompat.getColor(context, R.color.dragonType)
        else -> return ContextCompat.getColor(context, R.color.normalType)
    }
}