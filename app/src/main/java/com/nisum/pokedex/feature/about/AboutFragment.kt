package com.nisum.pokedex.feature.about

import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.FragmentAboutBinding
import com.nisum.pokedex.feature.MainViewModel
import com.nisum.pokedex.feature.common.BaseFragment

class AboutFragment : BaseFragment<FragmentAboutBinding>() {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun initView() {
        Glide.with(this).load(R.raw.pixel_run).into(binding.ivAboutAnimation)
    }

    override fun initListeners() {
        binding.ivLogoMenu.setOnClickListener {
            mainViewModel.openMenuDrawer()
        }
    }

    override fun initObservables() {}

    override fun getParametersFragment() {}

    override fun getViewBinding(): FragmentAboutBinding =
        FragmentAboutBinding.inflate(layoutInflater)

}