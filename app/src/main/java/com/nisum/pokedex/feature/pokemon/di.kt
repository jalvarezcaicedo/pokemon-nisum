package com.nisum.pokedex.feature.pokemon

import com.nisum.pokedex.data.repository.PokemonRepository
import com.nisum.usecase.pokemon.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class PokemonModule {

    @Provides
    @ViewModelScoped
    fun getRemotePokemonUseCase(
        pokemonRepository: PokemonRepository
    ) = GetRemotePokemonUseCase(pokemonRepository)

    @Provides
    @ViewModelScoped
    fun getRemotePokemonLocationsUseCase(
        pokemonRepository: PokemonRepository
    ) = GetRemotePokemonLocationsUseCase(pokemonRepository)

    @Provides
    @ViewModelScoped
    fun getRemotePokemonEvolveChainUseCase(
        pokemonRepository: PokemonRepository
    ) = GetRemotePokemonEvolveChainUseCase(pokemonRepository)

    @Provides
    @ViewModelScoped
    fun getRemotePokemonEvolveUseCase(
        pokemonRepository: PokemonRepository
    ) = GetRemotePokemonEvolveUseCase(pokemonRepository)

    @Provides
    @ViewModelScoped
    fun savePokemonDataUseCase(
        pokemonRepository: PokemonRepository
    ) = SavePokemonDataUseCase(pokemonRepository)

    @Provides
    @ViewModelScoped
    fun getLocalPokemonUseCase(
        pokemonRepository: PokemonRepository
    ) = GetLocalPokemonUseCase(pokemonRepository)

}