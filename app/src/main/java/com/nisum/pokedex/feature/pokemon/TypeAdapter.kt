package com.nisum.pokedex.feature.pokemon

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.nisum.domain.PokemonDomain
import com.nisum.domain.common.TypeData
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.ItemTypeBinding

class TypeAdapter : RecyclerView.Adapter<TypeAdapter.PokemonTypeViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<TypeData>() {
        override fun areItemsTheSame(oldItem: TypeData, newItem: TypeData): Boolean {
            return oldItem.type.name == newItem.type.name
        }

        override fun areContentsTheSame(oldItem: TypeData, newItem: TypeData): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonTypeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_type, parent, false)
        return PokemonTypeViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonTypeViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submitPokemonTypeData(pokemonDomain: PokemonDomain) {
        differ.submitList(pokemonDomain.types)
        notifyDataSetChanged()
    }

    inner class PokemonTypeViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemTypeBinding.bind(view)

        fun bind(item: TypeData) {
            binding.tvTypeName.text = item.type.name
            binding.cvItemTypeContainer.setCardBackgroundColor(
                getTypeColor(
                    view.context,
                    item.type.name
                )
            )
        }

    }
}