package com.nisum.pokedex.feature.pokedex

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.BuildConfig
import com.nisum.pokedex.R
import com.nisum.pokedex.databinding.ItemPokedexBinding
import com.nisum.pokedex.ktx.parseToViewId

class PokedexAdapter(
    private val listener: PokedexItemListener
) : RecyclerView.Adapter<PokedexAdapter.PokedexGridViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<PokedexDomain>() {
        override fun areItemsTheSame(oldItem: PokedexDomain, newItem: PokedexDomain): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PokedexDomain, newItem: PokedexDomain): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokedexGridViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_pokedex, parent, false)
        return PokedexGridViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokedexGridViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    fun submitPokedexData(pokedexData: List<PokedexDomain>) {
        differ.submitList(pokedexData)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class PokedexGridViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemPokedexBinding.bind(view)

        fun bind(item: PokedexDomain) {
            binding.clPokedexItemContainer.setOnClickListener {
                listener.onSelectProductListener(
                    item
                )
            }

            Glide.with(view).load(
                BuildConfig.POKEMON_IMAGE_URL_BASE.plus(item.id).plus(".png")
            ).centerCrop().into(binding.ivPokemonImage)

            binding.tvPokemonId.text = item.id.toString().parseToViewId()
            binding.tvPokemonName.text = item.name
        }
    }

}