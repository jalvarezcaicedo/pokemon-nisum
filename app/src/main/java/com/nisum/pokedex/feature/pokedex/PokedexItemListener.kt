package com.nisum.pokedex.feature.pokedex

import com.nisum.domain.PokedexDomain

interface PokedexItemListener {
    fun onSelectProductListener(pokemon: PokedexDomain)
}