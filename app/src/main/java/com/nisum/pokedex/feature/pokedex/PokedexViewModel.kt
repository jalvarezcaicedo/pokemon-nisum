package com.nisum.pokedex.feature.pokedex

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nisum.domain.Error
import com.nisum.domain.PokedexDomain
import com.nisum.usecase.pokedex.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokedexViewModel @Inject constructor(
    private val savePokedexData: SavePokedexDataUseCase,
    private val getLocalPokedexByIdUseCase: GetLocalPokedexByIdUseCase,
    private val getLocalPokedexByNameUseCase: GetLocalPokedexByNameUseCase,
    private val getLocalPokedexFilteredUseCase: GetLocalPokedexFilteredUseCase,
    private val getRemotePokedexUseCase: GetRemotePokedexUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getRemotePokedexData() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemotePokedexUseCase().fold(
            ifLeft = { error ->
                _state.update { it.copy(error = error, loading = false) }
            },
            ifRight = { response ->
                _state.update { it.copy(remotePokedex = response, loading = false) }
            }
        )
    }

    fun getPokedexFiltered(query: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalPokedexFilteredUseCase(query).catch {}.collect { pokedex ->
            _state.update { UIState(pokedexFiltered = pokedex) }
        }
    }

    fun getPokedexById() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalPokedexByIdUseCase().catch {}.collect { pokedex ->
            _state.update { UIState(pokedexById = pokedex) }
        }
    }

    fun getPokedexByName() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalPokedexByNameUseCase().catch {}.collect { pokedex ->
            _state.update { UIState(pokedexByName = pokedex) }
        }
    }

    fun savePokedexData(pokedexData: List<PokedexDomain>) = viewModelScope.launch {
        savePokedexData.invoke(pokedexData)
    }


    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val remotePokedex: List<PokedexDomain>? = null,
        val pokedexById: List<PokedexDomain>? = null,
        val pokedexByName: List<PokedexDomain>? = null,
        val pokedexFiltered: List<PokedexDomain>? = null
    )
}