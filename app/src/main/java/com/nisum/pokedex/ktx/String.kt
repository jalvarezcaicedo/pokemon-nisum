package com.nisum.pokedex.ktx

fun String.parseToViewId() = this.padStart(3, '0')