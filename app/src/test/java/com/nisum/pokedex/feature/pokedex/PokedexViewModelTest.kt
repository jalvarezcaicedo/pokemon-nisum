package com.nisum.pokedex.feature.pokedex

import arrow.core.right
import com.nisum.domain.PokedexDomain
import com.nisum.lib.pokedexMocked
import com.nisum.pokedex.CoroutinesTestRules
import com.nisum.usecase.pokedex.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PokedexViewModelTest {

    @get:Rule
    val coroutinesTestRules = CoroutinesTestRules()

    @Mock
    lateinit var savePokedexDataUseCase: SavePokedexDataUseCase

    @Mock
    lateinit var getLocalPokedexByIdUseCase: GetLocalPokedexByIdUseCase

    @Mock
    lateinit var getLocalPokedexByNameUseCase: GetLocalPokedexByNameUseCase

    @Mock
    lateinit var getLocalPokedexFilteredUseCase: GetLocalPokedexFilteredUseCase

    @Mock
    lateinit var getRemotePokedexUseCase: GetRemotePokedexUseCase

    private lateinit var pokedexViewModel: PokedexViewModel

    @Before
    fun setup() {
        pokedexViewModel = PokedexViewModel(
            savePokedexDataUseCase,
            getLocalPokedexByIdUseCase,
            getLocalPokedexByNameUseCase,
            getLocalPokedexFilteredUseCase,
            getRemotePokedexUseCase
        )
    }

    @Test
    fun `validate pokedex data from remote`() = runTest {
        val pokedexListMocked = mutableListOf<PokedexDomain>()
        pokedexListMocked.add(pokedexMocked.copy(id = 150))
        pokedexListMocked.add(pokedexMocked.copy(id = 151))
        whenever(getRemotePokedexUseCase()).thenReturn(pokedexListMocked.right())

        pokedexViewModel.getRemotePokedexData()

        val results = mutableListOf<PokedexViewModel.UIState>()
        val job = launch { pokedexViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        Assert.assertEquals(PokedexViewModel.UIState(remotePokedex = pokedexListMocked), results)
    }

    @Test
    fun `validate pokedex by id data from local`() = runTest {
        val pokedexListMocked = flowOf(listOf(pokedexMocked.copy(id = 150)))
        whenever(getLocalPokedexByIdUseCase()).thenReturn(pokedexListMocked)

        val result = pokedexViewModel.getPokedexById()
        Assert.assertEquals(pokedexListMocked, result)
    }

    @Test
    fun `validate pokedex by name data from local`() = runTest {
        val pokedexListMocked = flowOf(listOf(pokedexMocked.copy(id = 150)))
        whenever(getLocalPokedexByNameUseCase()).thenReturn(pokedexListMocked)

        val result = pokedexViewModel.getPokedexByName()
        Assert.assertEquals(pokedexListMocked, result)
    }

    @Test
    fun `validate pokedex filtered data from local`() = runTest {
        val queryMockRequest = "queryTest"
        val pokedexListMocked = flowOf(listOf(pokedexMocked.copy(id = 150)))
        whenever(getLocalPokedexFilteredUseCase(queryMockRequest)).thenReturn(pokedexListMocked)

        val result = pokedexViewModel.getPokedexFiltered(queryMockRequest)
        Assert.assertEquals(pokedexListMocked, result)
    }
}