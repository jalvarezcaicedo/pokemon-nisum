package com.nisum.pokedex.feature.pokemon

import arrow.core.right
import com.nisum.lib.pokemonMocked
import com.nisum.pokedex.CoroutinesTestRules
import com.nisum.usecase.pokemon.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PokemonViewModelTest {

    @get:Rule
    val coroutinesTestRules = CoroutinesTestRules()

    @Mock
    lateinit var getRemotePokemonUseCase: GetRemotePokemonUseCase

    @Mock
    lateinit var getRemotePokemonLocationsUseCase: GetRemotePokemonLocationsUseCase

    @Mock
    lateinit var getRemotePokemonEvolveChainUseCase: GetRemotePokemonEvolveChainUseCase

    @Mock
    lateinit var getRemotePokemonEvolveUseCase: GetRemotePokemonEvolveUseCase

    @Mock
    lateinit var getLocalPokemonUseCase: GetLocalPokemonUseCase

    @Mock
    lateinit var savePokemonDataUseCase: SavePokemonDataUseCase

    private lateinit var pokemonViewModel: PokemonViewModel

    @Before
    fun setup() {
        pokemonViewModel = PokemonViewModel(
            getRemotePokemonUseCase,
            getRemotePokemonLocationsUseCase,
            getRemotePokemonEvolveChainUseCase,
            getRemotePokemonEvolveUseCase,
            getLocalPokemonUseCase,
            savePokemonDataUseCase
        )
    }

    @Test
    fun `validate pokemon data from remote`() = runTest {
        val pokemonRequestDataMocked = pokemonMocked.copy(id = 100)
        val pokemonIdMocked = pokemonMocked.copy(id = 60)
        val pokemonDataMocked = pokemonMocked.copy(id = 100)
        whenever(getRemotePokemonUseCase(100)).thenReturn(pokemonDataMocked.right())
        whenever(getRemotePokemonLocationsUseCase(pokemonRequestDataMocked)).thenReturn(
            pokemonDataMocked.right()
        )
        whenever(getRemotePokemonEvolveChainUseCase(100)).thenReturn(pokemonIdMocked.id.right())
        whenever(
            getRemotePokemonEvolveUseCase(
                pokemonRequestDataMocked,
                pokemonIdMocked.id
            )
        ).thenReturn(
            pokemonDataMocked.right()
        )

        pokemonViewModel.getRemotePokemonData(100)

        val results = mutableListOf<PokemonViewModel.UIState>()
        val job = launch { pokemonViewModel.state.toList(results) }
        runCurrent()
        job.cancel()
        Assert.assertEquals(
            PokemonViewModel.UIState(remotePokemonData = pokemonDataMocked),
            results
        )
    }

    @Test
    fun `validate pokemon data from local`() = runTest {
        val pokemonMocked = flowOf(pokemonMocked.copy(id = 150))
        whenever(getLocalPokemonUseCase(150)).thenReturn(pokemonMocked)

        val result = pokemonViewModel.getLocalPokemonData(150)
        Assert.assertEquals(pokemonMocked, result)
    }

}