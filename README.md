# Pokemon-nisum

A simple project to show abilities in Nisum challenge taken data
from [PokeApi.co](https://pokeapi.co/) based on Kotlin MVVM with clean architecture as a framework
User interfaces based on figma Pokedex interface [Pokédex](https://www.figma.com/file/UrBsG0Izw31r9EEEaXH9oo/Pok%C3%A9dex-(Community)?node-id=0%3A1&t=kZx5y4GyYbZiCxYk-1)

## Architecture

Based on mvvm architecture with event communication through UI states, use cases and repository pattern based on
clean architecture.<br><br>
![architecture](https://user-images.githubusercontent.com/24237865/44525736-e9e7b700-a71c-11e8-8045-42c4478dd67e.png)

## Components

- MVVM Architecture
- Architecture Components (Lifecycle, ViewModel, Room Persistence, Flow, Navigation, Coroutines)
- ViewBinding
- The RESTful Pokémon API
- [Hilt](https://dagger.dev/hilt/) for dependency injection
- [Retrofit2 & Gson](https://github.com/square/retrofit) for constructing the REST API
- [Room](https://developer.android.com/jetpack/androidx/releases/room) for abstraction layer in data persistence
- [OkHttp3](https://github.com/square/okhttp) for implementing interceptor, logging and mocking web
  server
- [Glide](https://github.com/bumptech/glide) for loading images
- [Mockito-kotlin](https://github.com/mockito/mockito-kotlin) for Junit mock test
- [Timber](https://github.com/JakeWharton/timber) for logging

## Features

- Pokedex UI -> Basic UI with recycler view showing first 151 pokedex selectable and simple finder
- Pokemon UI -> Detailed UI with the selected pokemon information with types data, evolutions, abilities, locations and movements
- About UI -> Simple UI with a basic info of developer
