package com.nisum.lib

import com.nisum.domain.PokedexDomain
import com.nisum.domain.PokemonDomain

val pokedexMocked = PokedexDomain(
    1,
    "pokeTest"
)

val pokemonMocked = PokemonDomain(
    1,
    "pokeTest",
    listOf(),
    "evolveTest",
    listOf(),
    listOf(),
    listOf()
)