package com.nisum.pokedex.data.source

import com.nisum.domain.PokemonDomain
import kotlinx.coroutines.flow.Flow

interface LocalPokemonDataSource {
    fun getLocalPokemonData(id: Int): Flow<PokemonDomain>
    suspend fun savePokemon(pokemon: PokemonDomain)
}