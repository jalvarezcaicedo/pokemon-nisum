package com.nisum.pokedex.data.source

import com.nisum.domain.PokedexDomain
import com.nisum.domain.common.Pokedex
import kotlinx.coroutines.flow.Flow

interface LocalPokedexDataSource {
    fun getLocalPokedexDataById(): Flow<List<PokedexDomain>>
    fun getLocalPokedexDataByName(): Flow<List<PokedexDomain>>
    fun getLocalPokedexDataFiltered(query: String): Flow<List<PokedexDomain>>
    suspend fun savePokedex(pokedex: List<PokedexDomain>)
}