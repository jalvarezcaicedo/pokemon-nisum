package com.nisum.pokedex.data.source

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain

interface RemotePokemonDataSource {

    suspend fun getPokemonData(id: Int): Either<Error, PokemonDomain>
    suspend fun getPokemonLocationData(pokemonDomain: PokemonDomain): Either<Error, PokemonDomain>
    suspend fun getPokemonEvolveData(pokemonDomain: PokemonDomain, chainEvolveId: Int): Either<Error, PokemonDomain>
    suspend fun getPokemonEvolveChainData(pokemonId: Int): Either<Error, Int>

}