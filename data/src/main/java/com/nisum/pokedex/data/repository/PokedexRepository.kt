package com.nisum.pokedex.data.repository

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.source.LocalPokedexDataSource
import com.nisum.pokedex.data.source.RemotePokedexDataSource
import kotlinx.coroutines.flow.Flow

class PokedexRepository(
    private val localPokedexDataSource: LocalPokedexDataSource,
    private val remotePokedexDataSource: RemotePokedexDataSource
) {

    suspend fun savePokedexData(pokedexData: List<PokedexDomain>) {
        localPokedexDataSource.savePokedex(pokedexData)
    }

    suspend fun getPokedexData(limit: Long): Either<Error, List<PokedexDomain>> {
        return remotePokedexDataSource.getPokedex(limit)
    }

    fun getLocalPokedexById(): Flow<List<PokedexDomain>> =
        localPokedexDataSource.getLocalPokedexDataById()

    fun getLocalPokedexByName(): Flow<List<PokedexDomain>> =
        localPokedexDataSource.getLocalPokedexDataByName()

    fun getLocalPokedexFiltered(query: String): Flow<List<PokedexDomain>> =
        localPokedexDataSource.getLocalPokedexDataFiltered(query)

}