package com.nisum.pokedex.data.source

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokedexDomain

interface RemotePokedexDataSource {

    suspend fun getPokedex(limit: Long): Either<Error, List<PokedexDomain>>

}