package com.nisum.pokedex.data.repository

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.source.LocalPokemonDataSource
import com.nisum.pokedex.data.source.RemotePokemonDataSource
import kotlinx.coroutines.flow.Flow

class PokemonRepository(
    private val localPokemonDataSource: LocalPokemonDataSource,
    private val remotePokemonDataSource: RemotePokemonDataSource
) {

    suspend fun savePokemonData(pokemonData: PokemonDomain) {
        localPokemonDataSource.savePokemon(pokemonData)
    }

    suspend fun getPokemonData(id: Int): Either<Error, PokemonDomain> {
        return remotePokemonDataSource.getPokemonData(id)
    }

    suspend fun getPokemonLocationData(pokemonData: PokemonDomain): Either<Error, PokemonDomain> {
        return remotePokemonDataSource.getPokemonLocationData(pokemonData)
    }

    suspend fun getPokemonEvolveChainData(pokemonId: Int): Either<Error, Int> {
        return remotePokemonDataSource.getPokemonEvolveChainData(pokemonId)
    }

    suspend fun getPokemonEvolveData(pokemonData: PokemonDomain, chainEvolveId: Int): Either<Error, PokemonDomain> {
        return remotePokemonDataSource.getPokemonEvolveData(pokemonData, chainEvolveId)
    }

    fun getLocalPokemonData(id: Int): Flow<PokemonDomain> =
        localPokemonDataSource.getLocalPokemonData(id)

}