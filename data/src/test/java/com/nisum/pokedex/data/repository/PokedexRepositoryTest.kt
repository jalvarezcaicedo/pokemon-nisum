package com.nisum.pokedex.data.repository

import arrow.core.right
import com.nisum.lib.pokedexMocked
import com.nisum.pokedex.data.source.LocalPokedexDataSource
import com.nisum.pokedex.data.source.RemotePokedexDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PokedexRepositoryTest {

    @Mock
    lateinit var localPokedexDataSource: LocalPokedexDataSource

    @Mock
    lateinit var remotePokedexDataSource: RemotePokedexDataSource

    lateinit var pokedexRepository: PokedexRepository

    @Before
    fun setUp() {
        pokedexRepository = PokedexRepository(localPokedexDataSource, remotePokedexDataSource)
    }

    @Test
    fun `get pokedex remote and save data`() = runTest {
        val listPokedexData = listOf(pokedexMocked.copy(id = 10))
        whenever(remotePokedexDataSource.getPokedex(151)).thenReturn(listPokedexData.right())

        pokedexRepository.getPokedexData(151)
        verify(localPokedexDataSource).savePokedex(listPokedexData)
    }

    @Test
    fun `validate pokedex local by id event`() = runTest {
        val listPokedexData = flowOf(listOf(pokedexMocked.copy(id = 10)))
        whenever(localPokedexDataSource.getLocalPokedexDataById()).thenReturn(listPokedexData)

        pokedexRepository.getLocalPokedexById()
        verify(localPokedexDataSource).getLocalPokedexDataById()
    }

    @Test
    fun `validate pokedex local by name event`() = runTest {
        val listPokedexData = flowOf(listOf(pokedexMocked.copy(id = 10)))
        whenever(localPokedexDataSource.getLocalPokedexDataByName()).thenReturn(listPokedexData)

        pokedexRepository.getLocalPokedexByName()
        verify(localPokedexDataSource).getLocalPokedexDataByName()
    }

    @Test
    fun `validate pokedex local filtered event`() = runTest {
        val listPokedexData = flowOf(listOf(pokedexMocked.copy(id = 10)))
        whenever(localPokedexDataSource.getLocalPokedexDataFiltered("test")).thenReturn(
            listPokedexData
        )

        pokedexRepository.getLocalPokedexFiltered("test")
        verify(localPokedexDataSource).getLocalPokedexDataFiltered("test")
    }

}