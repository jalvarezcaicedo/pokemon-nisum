package com.nisum.pokedex.data.repository

import arrow.core.right
import com.nisum.lib.pokemonMocked
import com.nisum.pokedex.data.source.LocalPokemonDataSource
import com.nisum.pokedex.data.source.RemotePokemonDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PokemonRepositoryTest {

    @Mock
    lateinit var localPokemonDataSource: LocalPokemonDataSource

    @Mock
    lateinit var remotePokemonDataSource: RemotePokemonDataSource

    lateinit var pokemonRepository: PokemonRepository

    @Before
    fun setUp() {
        pokemonRepository = PokemonRepository(localPokemonDataSource, remotePokemonDataSource)
    }

    @Test
    fun `get pokemon remote and save data`() = runTest {
        val pokemonData = pokemonMocked.copy(id = 10)
        whenever(remotePokemonDataSource.getPokemonData(10)).thenReturn(pokemonData.right())

        pokemonRepository.getPokemonData(10)
        verify(localPokemonDataSource).savePokemon(pokemonData)
    }

    @Test
    fun `validate pokemon local event`() = runTest {
        val pokemonData = flowOf(pokemonMocked.copy(id = 10))
        whenever(localPokemonDataSource.getLocalPokemonData(10)).thenReturn(pokemonData)

        pokemonRepository.getLocalPokemonData(10)
        verify(localPokemonDataSource).getLocalPokemonData(10)
    }
}