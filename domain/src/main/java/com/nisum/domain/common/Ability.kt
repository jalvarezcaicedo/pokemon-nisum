package com.nisum.domain.common

data class AbilityData(
    val ability: Ability
)

data class Ability(
    val name: String
)
