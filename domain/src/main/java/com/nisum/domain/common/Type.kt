package com.nisum.domain.common

data class TypeData(
    val type: Type
)

data class Type(
    val name: String
)