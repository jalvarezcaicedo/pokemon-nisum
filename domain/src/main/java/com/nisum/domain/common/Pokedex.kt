package com.nisum.domain.common

data class Pokedex(
    var name: String,
    var url: String
)
