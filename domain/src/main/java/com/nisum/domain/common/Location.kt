package com.nisum.domain.common

data class Location(
    val name: String
)
