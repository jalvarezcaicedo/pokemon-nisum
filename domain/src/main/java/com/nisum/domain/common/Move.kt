package com.nisum.domain.common

data class Moves(
    val move: Move
)

data class Move(
    val name: String,
    val url: String
)
