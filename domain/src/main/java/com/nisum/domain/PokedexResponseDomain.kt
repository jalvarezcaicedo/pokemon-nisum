package com.nisum.domain

import com.nisum.domain.common.Pokedex

data class PokedexResponseDomain(
    var results: List<Pokedex>
)