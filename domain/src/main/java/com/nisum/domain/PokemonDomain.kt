package com.nisum.domain

import com.nisum.domain.common.AbilityData
import com.nisum.domain.common.Location
import com.nisum.domain.common.Moves
import com.nisum.domain.common.TypeData

data class PokemonDomain(
    var id: Int,
    var name: String,
    var abilities: List<AbilityData>,
    var evolve: String?,
    var locationArea: List<Location>?,
    var types: List<TypeData>,
    var moves: List<Moves>
)
