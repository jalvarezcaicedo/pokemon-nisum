package com.nisum.domain

data class PokedexDomain(
    val id: Long,
    val name: String
)