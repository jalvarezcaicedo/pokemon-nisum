package com.nisum.usecase.pokemon

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.repository.PokemonRepository

class GetRemotePokemonUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(id: Int): Either<Error, PokemonDomain> =
        pokemonRepository.getPokemonData(id)
}