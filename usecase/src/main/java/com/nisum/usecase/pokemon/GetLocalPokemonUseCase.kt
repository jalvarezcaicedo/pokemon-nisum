package com.nisum.usecase.pokemon

import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.repository.PokemonRepository
import kotlinx.coroutines.flow.Flow

class GetLocalPokemonUseCase(
    private val pokemonRepository: PokemonRepository
) {
    operator fun invoke(id: Int): Flow<PokemonDomain> =
        pokemonRepository.getLocalPokemonData(id)
}