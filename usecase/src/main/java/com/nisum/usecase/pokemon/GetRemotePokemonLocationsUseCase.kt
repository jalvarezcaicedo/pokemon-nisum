package com.nisum.usecase.pokemon

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.repository.PokemonRepository

class GetRemotePokemonLocationsUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(pokemonDomain: PokemonDomain): Either<Error, PokemonDomain> =
        pokemonRepository.getPokemonLocationData(pokemonDomain)
}