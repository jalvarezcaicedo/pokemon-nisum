package com.nisum.usecase.pokemon

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.pokedex.data.repository.PokemonRepository

class GetRemotePokemonEvolveChainUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(pokemonId: Int): Either<Error, Int> =
        pokemonRepository.getPokemonEvolveChainData(pokemonId)
}