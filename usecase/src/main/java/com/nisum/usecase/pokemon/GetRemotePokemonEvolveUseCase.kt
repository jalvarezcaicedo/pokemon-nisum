package com.nisum.usecase.pokemon

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.repository.PokemonRepository

class GetRemotePokemonEvolveUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(pokemonDomain: PokemonDomain, chainEvolveId: Int): Either<Error, PokemonDomain> =
        pokemonRepository.getPokemonEvolveData(pokemonDomain, chainEvolveId)
}