package com.nisum.usecase.pokemon

import com.nisum.domain.PokedexDomain
import com.nisum.domain.PokemonDomain
import com.nisum.pokedex.data.repository.PokedexRepository
import com.nisum.pokedex.data.repository.PokemonRepository

class SavePokemonDataUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(pokemonData: PokemonDomain) =
        pokemonRepository.savePokemonData(pokemonData)
}