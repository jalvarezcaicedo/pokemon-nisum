package com.nisum.usecase.pokedex

import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.repository.PokedexRepository
import kotlinx.coroutines.flow.Flow

class GetLocalPokedexFilteredUseCase(
    private val pokedexRepository: PokedexRepository
) {
    operator fun invoke(query: String): Flow<List<PokedexDomain>> =
        pokedexRepository.getLocalPokedexFiltered(query)
}