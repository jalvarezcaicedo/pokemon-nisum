package com.nisum.usecase.pokedex

import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.repository.PokedexRepository
import kotlinx.coroutines.flow.Flow

class GetLocalPokedexByIdUseCase(
    private val pokedexRepository: PokedexRepository
) {
    operator fun invoke(): Flow<List<PokedexDomain>> = pokedexRepository.getLocalPokedexById()
}