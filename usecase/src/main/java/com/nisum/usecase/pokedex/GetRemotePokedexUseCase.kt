package com.nisum.usecase.pokedex

import arrow.core.Either
import com.nisum.domain.Error
import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.repository.PokedexRepository

class GetRemotePokedexUseCase(
    private val pokedexRepository: PokedexRepository
) {
    suspend operator fun invoke(): Either<Error, List<PokedexDomain>> =
        pokedexRepository.getPokedexData(151)
}