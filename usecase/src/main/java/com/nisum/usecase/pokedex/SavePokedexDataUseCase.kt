package com.nisum.usecase.pokedex

import com.nisum.domain.PokedexDomain
import com.nisum.pokedex.data.repository.PokedexRepository

class SavePokedexDataUseCase(
    private val pokedexRepository: PokedexRepository
) {
    suspend operator fun invoke(pokedexData: List<PokedexDomain>) =
        pokedexRepository.savePokedexData(pokedexData)
}